#ifndef VK
#define VK
#include "urls.hpp"
#include <string>
#include <cctype> //need to
#include <iomanip>//url
#include <sstream>//encode


extern string token;
namespace vk{
  string anyMethod(string _method, string _args); // executing any method
  void urlEncode(string &_s);
  class wall{
  public:
    static void post(string _msg, string _att = "");
    static void pin(string _id);
    static void unpin();
  };
  class status{
  public:
    static void set(string _text, string _id = "");
  };
}
#endif // VK
