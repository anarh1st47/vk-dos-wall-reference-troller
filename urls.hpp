#ifndef URLS
#define URLS
#include <iostream>
#include <curl/curl.h>
#include <cstring>
#include <sstream>

using namespace std;

class urls{ 

static size_t WriteCallback(void *contents, size_t size, size_t nmemb, void *userp){
    ((std::string*)userp)->append((char*)contents, size *nmemb);
    return size *nmemb;
}

public:
  static string get(string _url){
    CURL *curl;
    CURLcode res;
    string readBuffer;
    //cout<<_url;
    curl = curl_easy_init();
    if(curl) {
      curl_easy_setopt(curl, CURLOPT_URL, _url.c_str());
      curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
      curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
      curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
      curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
      res = curl_easy_perform(curl);
      curl_easy_cleanup(curl);

      //cout<<endl<<endl<<readBuffer<<endl;
      return readBuffer;
    }
  }
};
#endif //URLS
