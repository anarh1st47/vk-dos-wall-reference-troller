#include "urls.hpp"
#include "vk.hpp"
#include <unistd.h> 

using namespace std;
using namespace vk;

string token;

int main(int argc, char *argv[]){
  urls vk;
  string id;
  if(argc != 2){
    cout<<"please enter only 1 arg"<<endl;
    return 1;
  }
  id = argv[1];
  token = "your token";
  ///wall::post("test trolling from c++ @id" + id + "(Недочеловек)");
  for(unsigned long long i = 0; i < 100000000; i++){
    string ans = anyMethod("wall.post", "message=Пора ставить GNU/Hurd, @id" + id + "(Недочеловек)");
    int start = ans.find("post_id")+9;
    int count = ans.find("}") - start;
    string post_id = ans.substr(start, count);
    sleep(3);
    ans = anyMethod("wall.delete", "post_id=" + post_id);
    sleep(3);
    cout<<ans<<endl;
  }
  
  //wall::unpin();
  //wall::pin("13351");
  //status::set("море*");
  //anyMethod("messages.send", "user_id=63432161&message=easy bleat from c++");
  return 0;
}
