#include "vk.hpp"



void vk::urlEncode(string &value) { 
    ostringstream escaped;
    escaped.fill('0');
    escaped << hex;

    for (string::const_iterator i = value.begin(), n = value.end(); i != n; ++i) {
        string::value_type c = (*i);

        // Keep alphanumeric and other accepted characters intact
        if (isalnum(c) || c == '-' || c == '_' || c == '.' || c == '~' || c == '=' || c == '&') {
            escaped << c;
            continue;
        }

        // Any other characters are percent-encoded
        escaped << uppercase;
        escaped << '%' << setw(2) << int((unsigned char) c);
        escaped << nouppercase;
    }
    value = escaped.str();
}

string vk::anyMethod(string _anyMethod, string _args){
  urlEncode(_args);
  urls https;
  string ans = https.get("https://api.vk.com/method/" + _anyMethod + "?" + _args + "&access_token=" + token);
  return ans;
}

void vk::wall::post(string _msg, string _att){
  string _kkk = anyMethod("wall.post", "message=" + _msg + "&attachments=" + _att);
}
void vk::wall::pin(string _id){
  string _kkk = anyMethod("wall.pin", "post_id=" + _id);
}
void vk::wall::unpin(){
  string _kkk = anyMethod("wall.unpin", "post_id=1");
}

void vk::status::set(string _text, string _id){
  string _kkk = anyMethod("status.set", "text=" + _text + "&group_id=" + _id);
}
